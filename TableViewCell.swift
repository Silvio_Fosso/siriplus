//
//  TableViewCell.swift
//  Siri
//
//  Created by Silvio Fosso on 20/03/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import UIKit
import IntentsUI
class TableViewCell: UITableViewCell {
   
    
    
    
    
    @IBOutlet weak var lb: UILabel!
    @IBOutlet weak var imgv: UIImageView!
   
    
   
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
   
    func addSiriButton(intent : INIntent) -> INUIAddVoiceShortcutButton {
        let button = INUIAddVoiceShortcutButton(style: .blackOutline)

        button.shortcut = INShortcut(intent: intent )
     
    return button
        
    }
    
}
