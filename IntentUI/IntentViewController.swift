//
//  IntentViewController.swift
//  IntentUI
//
//  Created by Silvio Fosso on 18/03/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import IntentsUI

// As an example, this extension's Info.plist has been configured to handle interactions for INSendMessageIntent.
// You will want to replace this or add other intents as appropriate.
// The intents whose interactions you wish to handle must be declared in the extension's Info.plist.

// You can test this example integration by saying things to Siri like:
// "Send a message using <myApp>"

class IntentViewController: UIViewController, INUIHostedViewControlling {
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
    }
        
    // MARK: - INUIHostedViewControlling
    
    
    // Prepare your view controller for the interaction to handle.
    func configureView(for parameters: Set<INParameter>, of interaction: INInteraction, interactiveBehavior: INUIInteractiveBehavior, context: INUIHostedViewContext, completion: @escaping (Bool, Set<INParameter>, CGSize) -> Void) {
        // Do configuration here, including preparing views and calculating a desired size for presentation.
        
        
       /* guard let intent = interaction.intent as? TopNewsIntent else {
            completion(true, Set(), .zero)
            return
        }*/
        let intent = interaction.intent
        switch intent {
        case is IngredientsIntent :
            let data = interaction.intentResponse?.userActivity?.userInfo
            let views = IngredientsViewController(for: intent as! IngredientsIntent)
            views.name = (data!["testo"] as? String)!
            //views.name = data!["testo"] as! String
            attachChild(views)
            completion(true, parameters, self.desiredSize)
            
        case is TopNewsIntent:
            let views = TopNewsview(for: intent as! TopNewsIntent)
            attachChild(views)
            completion(true, parameters, self.desiredSize)
            break
            
            
        default:
            break
        }
        
       
        
    }
    
    var desiredSize: CGSize {
        return self.extensionContext!.hostedViewMaximumAllowedSize
    }
    
    private func attachChild(_ viewController: UIViewController) {
        addChild(viewController)
        if let subview = viewController.view {
            view.addSubview(subview)
            subview.translatesAutoresizingMaskIntoConstraints = false
            
            // Set the child controller's view to be the exact same size as the parent controller's view.
            subview.widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
            subview.heightAnchor.constraint(equalTo: view.heightAnchor).isActive = true
            
            subview.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
            subview.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        }
        
        viewController.didMove(toParent: self)
    }
}

