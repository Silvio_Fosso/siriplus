//
//  IntentHandler.swift
//  Intent
//
//  Created by Silvio Fosso on 18/03/2020.
//  Copyright © 2020 Silvio Fosso. All rights reserved.
//

import Intents


class IntentHandler: INExtension {
  
    override func handler(for intent: INIntent) -> Any {
       
        switch intent {
        case is IngredientsIntent:
            return IngredientsHandler()
        
        case is TopNewsIntent:
            return TopNewsHandler()
        
        default:
            return (Any).self
        }
       
        
    }
    
 
}
